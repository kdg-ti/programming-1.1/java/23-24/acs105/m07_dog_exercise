import animals.Dog;
import animals.Rabbit;

public class AnimalTest {
	public static void main(String[] args) {
		System.out.printf("Animals: \n %s \n %s\n %s",
			new Dog("Ramses", "Border Collie", "black", "5522"),
			new Rabbit("Floppy", "Angora", "gray", true),
			new Rabbit("Peter", "Flemish giant", "gray", false)
		);
	}  // main()
}