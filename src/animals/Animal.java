package animals;

public class Animal {
	protected String name;
	protected String breed;
	protected String colour;
	protected String tagLine ;

	public Animal(String name, String breed, String colour, String tagLine) {
		this.name = name;
		this.breed = breed;
		this.colour = colour;
		this.tagLine = tagLine;
	}

	public String getName() {
		return name;
	}

	public String getBreed() {
		return breed;
	}

	public String getColour() {
		return colour;
	}

	public String getTagLine() {
		return tagLine;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	@Override
	public String toString() {
		// exercise 3
		//return String.format("%s is a %s (%s)", name, getClass().getSimpleName(), tagLine);
		// exercise 7
		return String.format("%s is a %s %s (%s)", name, colour, getClass().getSimpleName().toLowerCase(),breed);
	}
}
