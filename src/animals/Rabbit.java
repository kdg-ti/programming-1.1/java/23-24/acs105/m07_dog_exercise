package animals;

public class Rabbit extends Animal {
	private boolean digs;

	public Rabbit(String name, String breed, String colour, boolean digs) {
		super(name, breed, colour, "Carrots are good for your eyes");
		this.digs = digs;
	}

	public String toString() {
		// with an if
//		if (digs) {
//			return String.format("%s that digs", super.toString());
//		} else {
//			return String.format("%s that does not dig", super.toString());
//		}
		// with a ternary expression
		return String.format("%s that %s",
			super.toString(),
			digs ? "digs" : "does not dig");
	}
}
